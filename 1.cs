using System;
using System.Collections.Generic;

class Book {
	public String name;
	public String author;
	public int year;
	public String ISBN;
};

class Library {
	private List<Book> books;

	public Library() {
		books = new List<Book>();
	}

	public void add_book(Book book) {
		books.Add(book);
	}

	public void remove_book(int idx) {
		--idx;
		if (idx < 0 || idx >= books.Count) {
			Console.WriteLine("Incorrect index!");
			return;
		}

		books.RemoveAt(idx);
	}

	public void print_library() {
		print_books(books);
	}

	public void print_books(List<Book> books) {
		Console.WriteLine("\nBooks:");
		for(int i = 0; i < books.Count; ++i) {
			Console.WriteLine(
				String.Format(
					"[{0:D}]: \n\tName: {1:S}\n\tAuthor: {2:S}\n\tYear: {3:D}\n\tISBN: {4:S}\n", i + 1, books[i].name, books[i].author, books[i].year, books[i].ISBN));
		}
	}

	public List<Book> find_by_author(String author) {
		return books.FindAll(book => book.author == author);
	}
	public List<Book> find_by_name(String name) {
		return books.FindAll(book => book.name == name);
	}
};

class Program {
	public static void Main(String[] args) {
		Library lib = new Library();
		while(true) {
			Console.WriteLine("\n\nLibrary application");
			Console.WriteLine("Select option:");
			Console.WriteLine("1. Print library");
			Console.WriteLine("2. Add book");
			Console.WriteLine("3. Remove book by index");
			Console.WriteLine("4. Find by author");
			Console.WriteLine("5. Find by name");
			Console.WriteLine("6. Exit");

			int input;
			if (!Int32.TryParse(Console.ReadLine(), out input)) {
				Console.WriteLine("Incorrect input!\n");
				continue;
			}
			switch (input) {
				case 1:
					lib.print_library();
					break;
				case 2:
					try_add_book(lib);
					break;
				case 3:
					Console.WriteLine("Select book index: ");
					int idx;
					if (!Int32.TryParse(Console.ReadLine(), out idx)) {
						Console.WriteLine("Incorrect number");
						break;
					}

					lib.remove_book(idx);
					break;
				case 4:
					Console.WriteLine("Write author name:");
					String author = Console.ReadLine();
					lib.print_books(lib.find_by_author(author));
					break;
				case 5:
					Console.WriteLine("Write book name:");
					String name = Console.ReadLine();
					lib.print_books(lib.find_by_name(name));
					break;
				case 6:
					return;
				default:
					Console.WriteLine("Incorrect option!");
					break;
			}
		}
	}
	
	public static void try_add_book(Library lib) {
		Book book = new Book();
		Console.WriteLine("Enter book name:\n");
		book.name = Console.ReadLine();
		Console.WriteLine("Enter book author:\n");
		book.author = Console.ReadLine();
		Console.WriteLine("Enter book year:\n");
		book.year = Int32.Parse(Console.ReadLine());
		Console.WriteLine("Enter book ISBN:\n");
		book.ISBN = Console.ReadLine();

		lib.add_book(book);
	}
}
