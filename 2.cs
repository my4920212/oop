using System;

class BankAccount {
	private long number;
	private double score;

	public BankAccount(long num, double s) {
		number = num;
		score = s;
	}

	public void AddMoney(double amount) {
		score += amount;
	}

	public bool WithdrawMoney(double amount) {
		if (amount > score)
			return false;

		score -= amount;
		return true;
	}

	public String GetInfo() {
		return String.Format("Account:\n\tNumber: {0:D}\n\tScore: {1:F}", number, score);
	}

	public void PrintInfo() {
		Console.WriteLine(GetInfo());
	}
};

class Program {
	public static void Main(String[] args) {
		BankAccount account = new BankAccount(12345457634763476, 1000.0);

		Console.WriteLine("Вивести дані про рахунок");
		account.PrintInfo();

		Console.WriteLine("");
		Console.WriteLine("Операції над рахунком");
		Console.WriteLine("");
		Console.WriteLine("Поповнення на 42.99");
		account.AddMoney(42.99);
		account.PrintInfo();
		Console.WriteLine("");

		if(account.WithdrawMoney(999999)) {
			Console.WriteLine("Зняли 999999");
		} else {
			Console.WriteLine("Неможливо зняти 999999");
		}
		account.PrintInfo();
		Console.WriteLine("");

		if(account.WithdrawMoney(78.56)) {
			Console.WriteLine("Зняли 78.56");
		} else {
			Console.WriteLine("Неможливо зняти 78.56");
		}
		account.PrintInfo();
		Console.WriteLine("");
	}
};
