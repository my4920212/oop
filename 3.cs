using System;

class Palindrom {
	public static bool IsPalindrom(String str) {
		int halfsize = str.Length / 2;
		for(int i = 0; i < halfsize; ++i)
			if (str[i] != str[str.Length - i - 1])
				return false;

		return true;
	}
};

class Program {
	public static void Main(String[] args) {
		
		Console.WriteLine("Enter words:\n");
		String input = Console.ReadLine();

		while(input != null) {
			String result = input == "" ? "an empty word" : 
				(Palindrom.IsPalindrom(input) ? "a palindrom" : "not a palindrom");
			Console.WriteLine(String.Format("'{0:S}' is {1:S}", input, result));

			input = Console.ReadLine();
		}
	}
}
