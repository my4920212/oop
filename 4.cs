using System;
using System.Drawing;
using System.Windows.Forms;

public partial class App : Form {
	private TextBox XBox;
	private TextBox YBox;
	private Label resultBox;
	private Button drawButton;
	private PictureBox pictureBox;

	private double graphScaleSize = 100;

	private Point coordsToBitmapCoords(double x, double y) {
		return new Point(
			Math.Clamp((int)(x / graphScaleSize * (double)(pictureBox.Width / 2) + pictureBox.Width / 2), 0, pictureBox.Width),
			Math.Clamp((int)(y / graphScaleSize * -(double)(pictureBox.Height / 2) + pictureBox.Height / 2), 0, pictureBox.Height));
	}

	public App()
	{
		XBox = new TextBox();
		XBox.Location = new Point(12, 12);
		XBox.Name = "XBox";
		XBox.Size = new Size(100, 20);
		XBox.TabIndex = 0;

		YBox = new TextBox();
		YBox.Location = new Point(12, 38);
		YBox.Name = "YBox";
		YBox.Size = new Size(100, 20);
		YBox.TabIndex = 1;

		resultBox = new Label();
		resultBox.Location = new Point(12, 90);
		resultBox.Name = "resultBox";
		resultBox.Size = new Size(100, 23);
		resultBox.TabIndex = 1;

		drawButton = new Button();
		drawButton.Location = new Point(12, 64);
		drawButton.Name = "drawButton";
		drawButton.Size = new Size(100, 23);
		drawButton.TabIndex = 2;
		drawButton.Text = "Draw";
		drawButton.UseVisualStyleBackColor = true;
		drawButton.Click += new EventHandler(drawButton_Click);

		pictureBox = new PictureBox();
		((System.ComponentModel.ISupportInitialize)(pictureBox)).BeginInit();
		pictureBox.Location = new Point(118, 12);
		pictureBox.Name = "pictureBox";
		pictureBox.Size = new Size(400, 300);
		pictureBox.TabIndex = 3;
		pictureBox.TabStop = false;

		ClientSize = new Size(530, 327);
		Controls.Add(pictureBox);
		Controls.Add(drawButton);
		Controls.Add(YBox);
		Controls.Add(XBox);
		Controls.Add(resultBox);
		Name = "App";
		Text = "Drawing Application";
		((System.ComponentModel.ISupportInitialize)(pictureBox)).EndInit();
		ResumeLayout(false);
		PerformLayout();
	}

	private void drawButton_Click(object sender, EventArgs e)
	{
		double x, y;
		if (!double.TryParse(XBox.Text, out x)) {
			MessageBox.Show("Incorrect x value!");
			return;
		}

		if(!double.TryParse(YBox.Text, out y)) {
			MessageBox.Show("Incorrect y value!");
			return;
		}

		Bitmap bitmap = new Bitmap(pictureBox.Width, pictureBox.Height);
		using (Graphics g = Graphics.FromImage(bitmap))
		{
			g.Clear(Color.White);

			Pen coordsPen = new Pen(Color.Black, 1);
			g.DrawLine(coordsPen, pictureBox.Width / 2, 0, pictureBox.Width / 2, pictureBox.Height);
			g.DrawLine(coordsPen, 0, pictureBox.Height / 2, pictureBox.Width, pictureBox.Height / 2);

			Pen pen = new Pen(Color.Red, 1);
			const int radius = 5;

			Point pointCoords = coordsToBitmapCoords(x, y);
			g.DrawEllipse(pen, pointCoords.X - radius, pointCoords.Y - radius, radius * 2, radius * 2);
		}

		pictureBox.Image = bitmap;

		String result;
		if(x > 0) {
			if(y > 0) {
				result = "1st quadrant";
			} else if (y < 0) {
				result = "4th quadrant";
			} else {
				result = "Between 1st and 4th quadrant";
			}
			
		} else if (x < 0) {
			if (y > 0) {
				result = "2nd quadrant";
			} else if (y < 0) {
				result = "3rd quadrant";
			} else {
				result = "Between 2nd and 3rd quadrant";
			}
		} else {
			if (y > 0) {
				result = "Between 1st and 2nd quadrant";
			} else if (y < 0) {
				result = "Between 3rd and 4th quadrant";
			} else {
				result = "At (0. 0)";
			}

		}
		resultBox.Text = result;
	}
}

class Program {
	public static void Main(String[] args) {
		Application.Run(new App());
	}
}
