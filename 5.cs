using System;
using System.Drawing;
using System.Windows.Forms;

public partial class App : Form {
	private TextBox leftNumber;
	private TextBox rightNumber;
	private ComboBox operatorBox;
	private Label resultBox;
	private Button resultButton;

	public App()
	{
		const int numberBoxWidth = 50;
		const int spacing = 12;
		const int operatorBoxWidth = 30;

		leftNumber = new TextBox();
		leftNumber.Location = new Point(spacing, spacing);
		leftNumber.Name = "leftNumber";
		leftNumber.Size = new Size(numberBoxWidth, 20);
		leftNumber.TabIndex = 0;

		operatorBox = new ComboBox();
		operatorBox.Location = new Point(numberBoxWidth + spacing * 2, spacing);
		operatorBox.Name = "YBox";
		operatorBox.Size = new Size(operatorBoxWidth, 20);
		operatorBox.DropDownWidth = 50;
		operatorBox.MaxDropDownItems = 4;
		operatorBox.TabIndex = 1;
		operatorBox.Items.AddRange(new String[] {"+", "-", "*", "/"});
		operatorBox.DropDownStyle = ComboBoxStyle.DropDownList;
		operatorBox.SelectedIndex = 0;

		rightNumber = new TextBox();
		rightNumber.Location = new Point(numberBoxWidth + operatorBoxWidth + spacing * 3, spacing);
		rightNumber.Name = "YBox";
		rightNumber.Size = new Size(numberBoxWidth, 20);
		rightNumber.TabIndex = 2;

		resultButton = new Button();
		resultButton.Location = new Point(spacing, 70);
		resultButton.Name = "resultButton";
		resultButton.Size = new Size(100, 23);
		resultButton.TabIndex = 3;
		resultButton.Text = "Calculate";
		resultButton.UseVisualStyleBackColor = true;
		resultButton.Click += new EventHandler(resultButton_Click);

		resultBox = new Label();
		resultBox.Location = new Point(spacing, 47);
		resultBox.Name = "resultBox";
		resultBox.Size = new Size(100, 23);
		resultBox.Text = "Result here";
		resultBox.TabIndex = 4;

		ClientSize = new Size(530, 327);
		Controls.Add(leftNumber);
		Controls.Add(rightNumber);
		Controls.Add(operatorBox);
		Controls.Add(resultBox);
		Controls.Add(resultButton);
		Name = "App";
		Text = "Calculator";
		ResumeLayout(false);
		PerformLayout();
	}

	private void resultButton_Click(object sender, EventArgs e)
	{
		double x, y;
		if (!double.TryParse(leftNumber.Text, out x)) {
			MessageBox.Show("Incorrect left value!");
			return;
		}

		if(!double.TryParse(rightNumber.Text, out y)) {
			MessageBox.Show("Incorrect right value!");
			return;
		}

		switch (operatorBox.SelectedItem) {
			case "+":
				resultBox.Text = (x + y).ToString();
				break;
			case "-":
				resultBox.Text = (x - y).ToString();
				break;
			case "*":
				resultBox.Text = (x * y).ToString();
				break;
			case "/":
				resultBox.Text = (x / y).ToString();
				break;
		}
	}
}

class Program {
	public static void Main(String[] args) {
		Application.Run(new App());
	}
}

